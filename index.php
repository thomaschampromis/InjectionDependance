<?php 

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include 'Task.php';
include 'TodoList.php';

$task = new Task('task');
$task1 = new Task('task1');
$task2 = new Task('task2');
$task3 = new Task('task3');

$tasks[] = $task;
$tasks[] = $task1;
$tasks[] = $task2;

$todo = new TodoList($tasks);
var_dump($todo->getTasks());

$todo->addTask($task3);

var_dump($todo->getTasks());
