<?php 


class TodoList {
    
    private array $tasks;

    public function __construct(array $task)
    {
        $this->tasks = $task;
    }

    /**
     * @return array
     */
    public function getTasks(): array
    {
        return $this->tasks;
    }

    public function addTask(Task $task){
        $this->tasks[] = $task;
        return $this;
    }

}