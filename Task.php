<?php 


class Task {

    private string $name;

    public function __construct(string $name){
        $this->name = $name;
        
    }
    
    public function getName(){
        return $this->name;
    }

    public function setName(string $name){
        $this->name = $name;
        return $this;
    }

    

}